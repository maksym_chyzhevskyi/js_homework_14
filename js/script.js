"use strict";

const changeThemeButton = document.querySelector(".change_theme_btn");
const mainTheme = document.querySelector(".main-theme")

changeThemeButton.addEventListener("click", function () {
    let linkToTheme = mainTheme.getAttribute("href")
    if (linkToTheme === "./css/style.css") {
        mainTheme.setAttribute("href", "./css/anothertheme.css")
        localStorage.setItem("style", "./css/anothertheme.css")
    } else {
        mainTheme.setAttribute("href", "./css/style.css")
        localStorage.setItem("style", "./css/style.css")
    }
})

function loadTheme() {
    let currentTheme = localStorage.getItem("style")
    if (!currentTheme) {
        localStorage.setItem("style", mainTheme.getAttribute("href"))
    } else {
        mainTheme.setAttribute("href", currentTheme)
    }
}
loadTheme()

